_SITE_DOMAIN = 'django admin site url'
_ROOT_DOMIAN = 'django admin root site url '
_USERNAME = 'username' 
_PASSWORD = 'password'
_ACTIONS = ['save','save_continue','search','form_extra_pages']
_ERROR_LOG_NAME = 'admin_error_log.csv'
_LOG_FILE_NAME = 'admin_log.csv'
_ERROR_CHECK_ONLY = True
