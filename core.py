import unittest
from local_config import _SITE_DOMAIN,_USERNAME,_PASSWORD,_ACTIONS,_ROOT_DOMIAN,_ERROR_CHECK_ONLY,_LOG_FILE_NAME,_ERROR_LOG_NAME
from selenium import webdriver
import csv
_SEARCH_STRING = 'institute'

class PavoCore(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox() 
        self.driver.maximize_window()
        self.current_link_count = 0 
        self.driver.implicitly_wait(3)
        #self.create_csv(_LOG_FILE_NAME)
        self.HEADER = ['Sl.No','Testing Url','Title','Action','Status']

    def create_csv(self,file_name):
        self.log_file = open(file_name, 'wb')
        self.log_file_row = csv.writer(self.log_file, delimiter=',',
                                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
        self.log_file_row.writerow(self.HEADER)


    def login_to_admin_site(self):

        """ 
        Based on config.py get username and password ,then login to site named _SITE_DOMAIN 

        """

        self.driver.get(_SITE_DOMAIN)
        elem =self.driver.find_element_by_id("id_username")
        elem.send_keys(_USERNAME)
        pswd = self.driver.find_element_by_id("id_password")
        pswd.send_keys(_PASSWORD)
        SUBMIT_BUTTON_XPATH = '//input[@type="submit" and @value="Log in"]'
        submit_login_form = self.driver.find_element_by_xpath(SUBMIT_BUTTON_XPATH)
        submit_login_form.submit()
        print "{0} , logged in !! Test Starts.. ".format(_USERNAME)

    def check_for_search_field(self,current_link):
        try:
            self.driver.find_element_by_name('q') 
            self.driver.find_element_by_name('q').send_keys(_SEARCH_STRING)
            self.driver.find_element_by_name('q').submit()
            self.get_title_and_log(current_link,action='Search')
        except Exception as e:
            self.get_title_and_log(current_link,action='No Search bar found')


    def tearDown(self):
        #self.log_file.close()
        self.driver.close()
