import unittest
import urllib
import re
import csv
import time
from local_config import _SITE_DOMAIN,_USERNAME,_PASSWORD,_ACTIONS,_ROOT_DOMIAN,_ERROR_CHECK_ONLY,_LOG_FILE_NAME,_ERROR_LOG_NAME
from bs4 import BeautifulSoup
from core import PavoCore

class AdminSiteChecker(PavoCore):

    def test_navigate_admin_site(self):
        self.login_to_admin_site()
        if _ERROR_CHECK_ONLY:
            log_file_name = _ERROR_LOG_NAME
            try:
                admin_log_file_data = open(_LOG_FILE_NAME,'rb')
            except IOError:
                print "Sorry unable to find log file, Please set _ERROR_LOG_NAME to be False in local_config.py"
            log_file_content = csv.reader(admin_log_file_data)
            _error_links = []
            for i,each_data in enumerate(log_file_content):
                if i == 0:
                    header_content = each_data
                    error_pos = [ i  for i,each_title in enumerate(header_content) if 'status' in each_title.lower()][0]
                    url_pos = [ i for i,each_title in enumerate(header_content) if 'url' in each_title.lower()][0]
                else:
                    if each_data[error_pos] == 'ERROR' and each_data[url_pos] not in _error_links: 
                        _error_links.append(each_data[url_pos])
            admin_home_page_href_links = self._get_home_page_links(_error_links)
        else:
            log_file_name = _LOG_FILE_NAME 
            admin_home_page_web_content = self.driver.page_source
            admin_home_page_href_links = self.parse_content_rows_links(admin_home_page_web_content,page_type='homepage')

        self.total_links = len(admin_home_page_href_links)
        self.debug_on = self.check_debug_on(self.driver.page_source)
        self.create_csv(log_file_name)
        self.link_processing = 1 
        for _each_link in admin_home_page_href_links:
            self.driver.get(_each_link)
            print "Progress : %s/%s" %(self.link_processing,self.total_links)
            self.get_title_and_log(_each_link,action='Click')
            self.dive_inside_each_home_page_links(_each_link)
            self.link_processing += 1
        self.log_file.close()

    def check_debug_on(self,web_content):

        """ 
        Return true , if debug toolbar found in Admin Site 

        """
        all_content_soup = BeautifulSoup(web_content)
        debug_toolbar_presens = all_content_soup.find(id='djDebug') 
        if len(debug_toolbar_presens) > 0:
            DEBUG_HEADER = ['Time(in ms)','SQLQuery','SQLQueryTime(in ms)','StaticFiles','Cache','Signals','Receiver']
            self.HEADER.extend(DEBUG_HEADER)
            return True
        else:
            return False

    def _get_home_page_links(self,error_links):
        _home_page_links = []
        for each_links in error_links:
            each_links_element = each_links.split('/')
            for index,href_attr in enumerate(each_links_element):
                if href_attr.isdigit():
                    _home_page_link = "/".join(each_links_element[:index])
                else:
                    pass
            _home_page_links.append(_home_page_link)
        return _home_page_links


    def parse_content_rows_links(self,web_content,page_type=None):

        """ 
            Django admin list models instances in a div called content-main and each instance stay in scope row or class row2 , 
            return all model instance , based on parent link be homepage or model tab  
        """

        all_content_soup = BeautifulSoup(web_content)
        main_content_soup = all_content_soup.find_all(id="content-main")[0]
        if page_type == 'homepage':
            link_div = main_content_soup.find_all(scope='row') 
        elif page_type == 'model_tab':
            link_div = main_content_soup.find_all(class_='row2') 
        _page_href_links = []
        for link in link_div:
            relative_href_link = link.a['href']
            _page_href_links.append(form_complete_url(relative_href_link))
        return _page_href_links


    def dive_inside_each_home_page_links(self,each_parent_link):

        """ 
        Opening each link and performing actions like SEARCH, SAVE , CONTUNUE EDITING , opening any other form 
        if available for each opened form on each links  

        """

        _page_content = self.driver.page_source
        first_parent_record_link = self.get_first_parent_record_link(_page_content)
        if not first_parent_record_link:
            if first_parent_record_link is None:
                first_parent_record_link = "No data found in model : %s" %(each_parent_link)
            self.get_title_and_log(first_parent_record_link,action='No Data in model ')
        else:
            self.driver.get(first_parent_record_link)
            self.get_title_and_log(first_parent_record_link,action='Form Opened')
            self.dive_inside_model_form(first_parent_record_link,each_parent_link)


    def dive_inside_model_form(self,first_parent_record_link,each_parent_link):

        if 'save' in _ACTIONS or 'save_continue' in _ACTIONS:
            self.driver.get(first_parent_record_link)
            save_status = self.save_continue_save_operation(first_parent_record_link)
            if not save_status:
                self.driver.get(each_parent_link)

        if 'search' in _ACTIONS :
            self.check_for_search_field(each_parent_link)

        if 'form_extra_pages' in _ACTIONS:
            ### check for model form custom view template and open the custom form , further perform continue_save and save operation   
            ## find element by class name object-tools , further find all href links except history/ and then perform actions 
            #of save and continue_save 
            #self.driver.get(first_parent_record_link)
            try:
                _form_extra_pages = self.driver.find_elements_by_class_name('historylink')
                form_extra_pages = [[_eachlink.text.lower() , _eachlink.get_attribute('href')] for _eachlink in _form_extra_pages] 
            except:
                form_extra_pages = []
            #self.driver.get(first_parent_record_link)
            for _each_tab_text_href in form_extra_pages:
                if 'history' not in _each_tab_text_href[0]:
                    form_extra_url = _each_tab_text_href[1] 
                    self.driver.get(form_extra_url)
                    extra_form_source = self.driver.page_source
                    extra_form_source_links = self.parse_content_rows_links(extra_form_source,page_type='model_tab')
                    first_link = extra_form_source_links[0]
                    self.driver.get(first_link)
                    save_continue_status = self.save_continue_save_operation(first_link)

    def save_continue_save_operation(self,model_form_url):
        """ 
        Perform save and continue editing operation in opend model form  

        """
        model_form_url_last_digit = model_form_url.split('/')[-1]
        if model_form_url.endswith('/'):
            last_dig = model_form_url.split('/')[-2]
        else:
            last_dig = model_form_url.split('/')[-1]
        if last_dig.isdigit():
            try:
                _continue_key = self.driver.find_element_by_name("_continue")
                _continue_key.submit()
                self.get_title_and_log(model_form_url,action='Save and Continue')
                _save_key = self.driver.find_element_by_name('_save')
                _save_key.submit()
                self.get_title_and_log(model_form_url,action='Save')
                if model_form_url == self.driver.current_url:
                    #self.get_title_and_log(model_form_url,action='Exception Occured in save')
                    #print "Exception Occured in save "
                    return False 
                else:
                    return True
            except Exception as e:
                self.get_title_and_log(model_form_url,action='Exception Occured in save')
                print "Exception Occured in save "
                return False

    def get_first_parent_record_link(self,_page_content):
        try:
            _page_content_soup = BeautifulSoup(_page_content)
            first_link_href = _page_content_soup.find_all(class_ = 'row1')[0].a['href']
            return form_complete_url(first_link_href) 
        except:
            pass

    def get_title_and_log(self,current_link,action=None):
        current_title = self.driver.title
        self.current_link_count += 1
        csv_data = [self.current_link_count,current_link,current_title,action]
        if 'error' in current_title.lower(): 
            csv_data.append('ERROR')
        else:
            csv_data.append('SUCCESS')
        if self.debug_on:
            debug_data = self.get_debug_data(self.driver)
            csv_data.extend(debug_data)
        self.log_file_row.writerow(csv_data)
        return True
        
    def get_debug_data(self,driver_obj):
        page_as_soup = BeautifulSoup(driver_obj.page_source)
        debug_toolbar_pannel= page_as_soup.find(id='djDebug')
        time = get_time_data(debug_toolbar_pannel)
        no_of_query, sql_time_taken = get_sql_data(debug_toolbar_pannel)
        no_of_static_files = get_static_data(debug_toolbar_pannel)
        cached_records = get_cached_data(debug_toolbar_pannel)
        signal_sender,signal_receiver = get_signal_data(debug_toolbar_pannel)
        debug_data = [time,no_of_query,sql_time_taken,no_of_static_files, cached_records, signal_sender, signal_receiver]
        return debug_data


def form_complete_url(_relative_href_link):

    """ 
        Based on hrefs links of in django admin site , form complete url starting with _SITE_DOMAIN , if _relative_href_link is itself a href link starting with http then it return itself us a complete_url  
    
    """
    if _relative_href_link.startswith('http') or _relative_href_link.startswith('www'):
        return _relative_href_link
    else:
        return _ROOT_DOMIAN + _relative_href_link 

def get_time_data(debug_toolbar_pannel):
    time_text = debug_toolbar_pannel.find(class_='TimerPanel').small.text
    time = get_int_from_str(time_text)
    return time

def get_sql_data(debug_toolbar_pannel):
    sql_text = debug_toolbar_pannel.find(class_='SQLPanel').small.text
    for each in sql_text.split( ):
        if each.isdigit():
            no_of_query  = int(each)
            break
    sql_time_taken = get_float_from_str(sql_text) 
    return no_of_query,sql_time_taken

def get_static_data(debug_toolbar_pannel):
    static_text = debug_toolbar_pannel.find(class_='StaticFilesPanel').small.text
    static_files_served = get_int_from_str(static_text)
    return static_files_served

def get_cached_data(debug_toolbar_pannel):
    cache_text = debug_toolbar_pannel.find(class_='CachePanel').small.text
    cache_call = get_int_from_str(cache_text)
    return cache_call

def get_signal_data(debug_toolbar_pannel):
    signal_text = debug_toolbar_pannel.find(class_='SignalsPanel').small.text
    count = 1
    for each in signal_text.split(' '):
        if each.isdigit() and count == 1:
            receiver_count = int(each)
            count += 1 
        if each.isdigit() and count == 2:
            signal_count = int(each)
    return signal_count, receiver_count

def get_float_from_str(text):
    return float(re.search("\d+.\d+", text).group())

def get_int_from_str(text):
    return int(re.search("\d+", text).group())

if __name__ == "__main__":
    unittest.main()
